const path = require("path");
const Promise = require('bluebird');

module.exports = function(app) {

    app.get("/", (req, res) => {
        Promise.try(() => {
            return req.session.passport || false;
        }).then((resp) => {
            res.render('index', resp)
        })
    })

    app.get("/views/login", (req, res) => {
        res.render('login')
    })

}