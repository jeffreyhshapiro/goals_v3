define('logout', () => {

    
    class Logout {
        constructor() {
            this.init();
        }
        
        init() {
            this.registerEvents();
        }
        
        registerEvents() {
            document.querySelector(".nav-item.logout").addEventListener("click", (e) => {
                e.preventDefault();

                console.log("erereer")
                
                axios.get("/api/logout")
                    .then(() => {
                        window.location.reload();
                    })
                    .catch(e => e);
                
                return false;
            }, true)
        }
    }

    return Logout;
});