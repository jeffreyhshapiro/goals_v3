define('signup', () => {

    class SignUp {
    
        constructor() {
            this.init();
        }
    
        init() {
            document.querySelector("#SignUpForm").addEventListener("submit", this.createNewUser);
            document.querySelector("#SignUpForm .login-link a").addEventListener("click", this.showLoginForm);
            
        }
    
        createNewUser(e) {
    
            e.preventDefault();
    
            const username = document.querySelector("#SignUpForm [name=username]").value
            const password = document.querySelector("#SignUpForm [name=password]").value
    
            const user = {
                emailAddress: username,
                password
            }
    
            if(username && password) {
                axios.post('/api/authenticate', user).then((res) => {
                    window.location.reload();
                })
                .catch(e => e);
            }
    
    
        }

        showLoginForm(e) {

            e.preventDefault();

            axios.get('/views/login').then((res) => {
                console.log(res)
            })

        }
    
    }

    return SignUp

})
