let modulesToLoad = ['goals', 'signup', 'logout']


function classesToInstantiate(c) {
    let currentClasses = [...document.querySelectorAll("[data-load-class]")];
    
    for (let i = 0; i < currentClasses.length; i++) {
        let className = currentClasses[i].getAttribute("data-load-class").toLowerCase();
        
        if (className === c) {
            return className === c;
        }
    }
}

function loader(...modules) {
    
    let modulesToLoad = [...modules];
    
    
    for (let i = 0; i < modulesToLoad.length; i++) {
        
        if (typeof modules[i] === "function" && classesToInstantiate(modulesToLoad[i].name.toLowerCase())) {
            new modules[i]();
        }
    }    
}

require(modulesToLoad, loader);